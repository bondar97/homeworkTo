import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Toggler, TogglerItem} from './toggler';



// class App extends Component {
//   state = {
//    left: false,
//    middle: true,
//    right: false
//  };

//   left = (event) => {
//     console.log( event, this.state );
//     let middle = this.state.middle === true ? false : false;
//     let right = this.state.right === true ? false : false;
//     let left = this.state.left === false ? true : false;
//     this.setState({ middle: middle, left: left, right: right });
//   }

//   middle = (event) => {
//     console.log( event, this.state );
//     let left = this.state.left === true ? false : false;
//     let right = this.state.right === true ? false : false;
//     let middle = this.state.middle === false ? true : false;
//     this.setState({ middle: middle, left: left, right: right });
//   }

//   right = (event) => {
//     console.log( event, this.state );
//     let left = this.state.left === true ? false : false;
//     let middle = this.state.middle === true ? false : false;
//     let right = this.state.right === false ? true : false;
//     this.setState({ middle: middle, left: left, right: right });
//   }

//   render() {
//     return (
//       <div className="App">
//       <h1 className={this.state.left === true ? "text-left maintext" : this.state.middle===true ? "text-middle maintext" : this.state.right===true ? "text-right maintext" :false}> Simple radio buttons</h1>

//         <div className="Toggler">
//           <button className={this.state.left === true ? "left-button button-active" : "left-button"} onClick={this.left}> Left </button>
//           <button className={this.state.middle === true ? "middle-button button-active" : "middle-button"} onClick={this.middle}> Middle </button>
//           <button className={this.state.right === true ? "right-button button-active" : "right-button"} onClick={this.right}> Right </button>
//         </div>
//       </div>
//     );
//   }
// }



class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      data: {
        name: "",
        activeToggler: "left",
        activeTogglerHeader: "left"
      }
    };
  }

  changeStatus = (event) => {
  	let TogglerValue = event.target.innerText;
  	this.setState({
  		data:{
  			...this.state.data,
  			activeToggler: TogglerValue,
  			activeTogglerHeader: TogglerValue
  		}});
  }

  render() {
  	let {activeToggler} = this.state.data;
  	let {activeTogglerHeader} = this.state.data;

    return (
      <div className="App">
        <Toggler
          activeTogglerHeader={activeToggler}
          changeStatus={this.changeStatus}
          name="This is my togler"
        > 
        </Toggler>

        <Toggler
          activeToggler={activeToggler}
          changeStatus={this.changeStatus}
        >
          <TogglerItem className="left" name="left"/>
          <TogglerItem className="middle" name="middle"/>
          <TogglerItem className="right" name="right"/>
        </Toggler>
      </div>
    );
  }
}


export default App;


