import React, { Component } from 'react';

export class Toggler extends Component {
  render(){
    let {name, children, activeToggler, changeStatus, activeTogglerHeader} = this.props;
    return(
      <div>
        <div className={`text-${activeTogglerHeader}`}>
        {name}
        </div>
        <div className="togglerContainer">
          {
            //https://reactjs.org/docs/react-api.html#reactchildren
            React.Children.map(
              children,
              (ChildrenItem) => {
                if(ChildrenItem.props.name === activeToggler){
                    // https://reactjs.org/docs/react-api.html#cloneelement
                    return React.cloneElement(ChildrenItem, {
                      name: ChildrenItem.props.name,
                      active: true,
                      changeStatus: changeStatus
                    })
                } else {
                  return React.cloneElement(ChildrenItem, {
                    name: ChildrenItem.props.name,
                    changeStatus: changeStatus
                  })
                }
              }
            )
            }
        </div>
      </div>
    );
  }
}

export const TogglerItem = ({name, active, changeStatus, activeTogglerHeader}) => {
  return(

    <div className={
      active === true ?
        `active ${name} text-left` :
        name
      }
      onClick={
        changeStatus !== undefined ?
          changeStatus :
          null
      }
      >
      {name}
    </div>
  );
};
